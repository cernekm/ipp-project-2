#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#MKA:xcerne01
#title           : mka.py
#author          : Martin Cernek
#email           : xcerne01(at)stud.fit.vutbr.cz
#date            : 16/4/2015
#version         : 1.01
#python_version  : 3.4.0
#usage           : python3 mka.py [--input=FILE] [--output=FILE] [--help] [-f|m] [-i]

import sys
import getopt
import fa

def print_help():
    """Function print help. """

    print("Usage: python3 mka.py [--input=FILE] [--output=FILE] [--help] [-f|m] [-i]\n" + \
          "Script provides processing of finite automaton from input.\n" + \
          "Options:\n" + \
          "\t-i --case-insensitive       convert all upper-case letters to lower-case\n" + \
          "\t-f --find-non-finishing     return nonterminating state of finite automaton\n" + \
          "\t-m --minimize               perform a minimization\n" + \
          "\t--help                      print help\n" + \
          "Standart streams are used without options --input and --output.\n" + \
          "For more information see documentation.\n" + \
          "(c) xcerne01@stud.fit.vutbr.cz")


def main(argv):
    """Main function of script which parses arguments from command line and performs operations associated with finite automaton.

    Args:
    argv -- arguments from command line

    Returns:
    0 -- succes 
    1 -- incorrect arguments from command line
    2 -- error with opening input file
    3 -- error with opening output file
    60 -- invalid syntax of finite automaton
    61 -- invalid  semantics of finite automaton
    62 -- not well specified finite automaton
    
    """

    # Control variables
    fflag = False;
    mflag = False;
    iflag = False;
    aflag = False;
    inputfilename = None
    outputfilename = None
    astring = ""
    # Parsing arguments from command line
    try:
        opts, args = getopt.getopt(argv, "fmi", ["help", "input=", "output=", "find-non-finishing", "minimize", "case-insensitive", "analyze-string="])
    except:
        sys.stderr.write("mka.py: invalid options\nTry '--help' for more information.\n")
        return 1
    if len(argv) - len(args) != len(opts):
        sys.stderr.write("mka.py: invalid options\nTry '--help' for more information.\n")
        return 1
    if args:
        sys.stderr.write("mka.py: invalid options\nTry '--help' for more information.\n")
        return 1
    for opt, value in opts:
        if opt not in argv and not value:
            sys.stderr.write("mka.py: invalid options\nTry '--help' for more information.\n")
            return 1
        if opt == "--help":
            if len(argv) != 1:
                sys.stderr.write("mka.py: invalid options\nTry '--help' for more information.\n")
                return 1
            print_help()
            return 0
        elif opt == "--input":
            if inputfilename:
                sys.stderr.write("mka.py: invalid options\nTry '--help' for more information.\n")
                return 1
            inputfilename = value;
        elif opt == "--output":
            if outputfilename:
                sys.stderr.write("mka.py: invalid options\nTry '--help' for more information.\n")
                return 1
            outputfilename = value;
        elif opt == "--find-non-finishing" or opt == "-f":
            if fflag:
                sys.stderr.write("mka.py: invalid options\nTry '--help' for more information.\n")
                return 1
            if mflag or aflag:
                sys.stderr.write("mka.py: invalid options\nTry '--help' for more information.\n")
                return 1
            fflag = True
        elif opt == "--minimize" or opt == "-m":
            if mflag:
                sys.stderr.write("mka.py: invalid options\nTry '--help' for more information.\n")
                return 1
            if fflag or aflag:
                sys.stderr.write("mka.py: invalid options\nTry '--help' for more information.\n")
                return 1
            mflag = True
        elif opt == "--case-insensitive" or opt == "-i":
            if iflag:
                sys.stderr.write("mka.py: invalid options\nTry '--help' for more information.\n")
                return 1
            iflag = True
        elif opt == "--analyze-string":
            if aflag:
                sys.stderr.write("mka.py: invalid options\nTry '--help' for more information.\n")
                return 1
            if mflag or fflag:
                sys.stderr.write("mka.py: invalid options\nTry '--help' for more information.\n")
                return 1
            aflag = True
            astring = value

    # Opening of input file
    try:
        if inputfilename:
            inputfile = open(inputfilename, "r")
        else:
            inputfile = sys.stdin
    except:
        sys.stderr.write("mka.py: " + inputfilename + ": no such file\n")
        return 2

    # Reading of content
    text = inputfile.read()
    # Closing of input file
    inputfile.close()

    try:
        tmp = fa.parse_input(text);
        finite_automaton = fa.FiniteAutomaton(tmp[0], tmp[1], tmp[2], tmp[3], tmp[4])
    except:
        sys.stderr.write("mka.py: invalid syntax of finite automaton\n")
        return 60

    # Option --case-insensitive
    if iflag:
        finite_automaton.to_case_insensitive()
        astring = astring.lower()

    if not finite_automaton.is_valid():
        sys.stderr.write("mka.py: invalid semantics of finite automaton\n")
        return 61
    if not finite_automaton.is_well_spec():
        sys.stderr.write("mka.py: finite automaton is not well specified\n")
        return 62

    # Opening of output file
    try:
        if outputfilename:
            outputfile = open(outputfilename, "w")
        else:
            outputfile = sys.stdout
    except:
        sys.stderr.write("mka.py: " + outputfilename + ": error in writing to file\n")
        return 3

    # Finding nonterminating state
    if fflag:
        outputfile.write(finite_automaton.get_non_finishing_state())
        # Closing of output file
        outputfile.close()
        return 0
    # Analyzing string
    elif aflag:
        try:
            outputfile.write(str(finite_automaton.analyze(astring)))
        except:
            sys.stderr.write("mka.py: invalid symbol in analyzing string\n")
            outputfile.close()
            return 1
        # Closing of output file
        outputfile.close()
        return 0
    # Perform minimization 
    elif mflag:
        finite_automaton.minimize()
    
    # Writing normal form of finite automaton into file
    finite_automaton.write(outputfile)

    # Closing output file
    outputfile.close()
    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))