#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#MKA:xcerne01
#title           : fa.py
#author          : Martin Cernek
#email           : xcerne01(at)stud.fit.vutbr.cz
#date            : 16/4/2015
#version         : 1.01
#python_version  : 3.4.0

import re
import sys


class FiniteAutomaton(object):
    """Class representing finite automaton.

    Attributes:
      STATES -- finite set of states
      INPUT_ALPHABET -- finite set of symbols
      RULES -- finite set of rules
      START_STATE -- start state
      FINAL_STATES -- finite set of final states
      NON_FINISHING_STATE -- nonterminating state of finite automaton

    """
    def __init__(self, states, input_alphabet, rules, start_state, final_states):
        self.STATES = states
        self.INPUT_ALPHABET = input_alphabet
        self.RULES = rules
        self.START_STATE = start_state
        self.FINAL_STATES = final_states
        self.NON_FINISHING_STATE = None

    def to_case_insensitive(self):
        """Methods convert all upper-case letters to lower case. """

        self.STATES = { tmp.lower() for tmp in self.STATES }
        self.INPUT_ALPHABET = { tmp.lower() for tmp in self.INPUT_ALPHABET }
        self.RULES = { tmp.lower() for tmp in self.RULES }
        self.START_STATE = self.START_STATE.lower()
        self.FINAL_STATES = { tmp.lower() for tmp in self.FINAL_STATES }
    
    def is_valid(self):
        """Methods check if semantic of finite automaton is correct.

        Returns:
        True -- if semantic is correct
        False -- if semantic is incorrect

        """

        # Input alphabet must not be empty.
        if not self.INPUT_ALPHABET:
            return False

        # Start state must be in set of states.
        if not self.START_STATE in self.STATES:
            return False

        # Set of final states must be subset of set of states.
        if not self.FINAL_STATES <= self.STATES:
            return False

        # Every state or symbol must be in set of states or in input alphabet.
        for tmp in self.RULES:
            tmp_state1 = re.search(r"^(.+?)'", tmp).group(1)
            if not tmp_state1 in self.STATES:
                return False
            tmp_symbol = re.search(r"('.{0,2}')", tmp, re.DOTALL).group(1)
            # Epsilon move
            if tmp_symbol == "''":
                pass
            elif not tmp_symbol in self.INPUT_ALPHABET:
                return False
            tmp_state2 = re.search(r"'->(.+)$", tmp).group(1)
            if not tmp_state2 in self.STATES:
                return False

        return True

    def is_well_spec(self):
        """Method check if finite automaton is well specified.

        Returns:
        True -- if is well specified
        False -- if is not well specified

        """

        # Epsilon moves
        for tmp in self.RULES:
            tmp_symbol = re.search(r"('.{0,2}')", tmp, re.DOTALL).group(1)
            if tmp_symbol == "''":
                return False

        # Determinism
        for tmp in self.RULES:
            tmp_left_side = re.search(r"^(.+?)->", tmp, re.DOTALL).group(1)
            for tmp2 in self.RULES - {tmp}:
                tmp_left_side2 = re.search(r"^(.+?)->", tmp2, re.DOTALL).group(1)
                # If exists more moves from state with the same symbol
                if tmp_left_side == tmp_left_side2:
                    return False

        # Accessible states
        tmp_states_list = [self.START_STATE]
        tmp_rules_set = self.RULES
        tmp_rules_diff_set = set()
        while tmp_states_list:
            tmp_state = tmp_states_list.pop()
            tmp_rules_diff_set.clear()
            for rule in tmp_rules_set:
                tmp_state1 = re.search(r"^(.+?)'", rule).group(1)
                tmp_state2 = re.search(r"'->(.+)$", rule).group(1)
                if tmp_state1 == tmp_state:
                    tmp_states_list.append(tmp_state2)
                    tmp_rules_diff_set.add(rule)
            tmp_rules_set = tmp_rules_set - tmp_rules_diff_set
        # Every state must be accessible
        if tmp_rules_set:
            return False

        # Terminating states
        tmp_states_list = list(self.FINAL_STATES)
        tmp_rules_set = self.RULES
        tmp_rules_diff_set = set()
        while tmp_states_list:
            tmp_rules_diff_set.clear()
            tmp_state = tmp_states_list.pop()
            for rule in tmp_rules_set:
                tmp_state1 = re.search(r"^(.+?)'", rule).group(1)
                tmp_state2 = re.search(r"'->(.+)$", rule).group(1)
                if tmp_state2 == tmp_state:
                    tmp_states_list.append(tmp_state1)
                    tmp_rules_diff_set.add(rule)
            tmp_rules_set = tmp_rules_set - tmp_rules_diff_set
        # Must have 0 or 1 terminating state
        if len({ re.search(r"'->(.+)$", tmp).group(1) for tmp in tmp_rules_set }) > 1:
            return False
        try:
            self.NON_FINISHING_STATE = { re.search(r"'->(.+)$", tmp).group(1) for tmp in tmp_rules_set }.pop()
        except:
            # When there is not terminating state
            self.NON_FINISHING_STATE = "0"

        # Completeness
        left_sides = { re.search(r"^(.+?)->", tmp, re.DOTALL).group(1) for tmp in self.RULES }
        # Every state must have move with every symbol from input alphabet
        for tmp_state in self.STATES:
            for tmp_symbol in self.INPUT_ALPHABET:
                if not (tmp_state + tmp_symbol) in left_sides:
                    return False

        return True

    def get_non_finishing_state(self):
        """Method return nonterminating state.

        Returns:
        NON_FINISHING_STATE

        """

        return self.NON_FINISHING_STATE

    def minimize(self):
        """Method minimize finite automaton. """

        # Make set from sets
        Q_m = { frozenset(self.FINAL_STATES), frozenset(self.STATES - self.FINAL_STATES) }
        # Temporary variables
        new_Q_m = set()
        tmp_X = set()
        # Indicates if set is subset another set
        issubset = False
        while True:
            # Indicates if states was modified
            changed = False
            for X in Q_m:
                for d in self.INPUT_ALPHABET:
                    # Finding rules
                    for p in X:
                        for rule in self.RULES:
                            if (p + d) == re.search(r"^(.+?)->", rule, re.DOTALL).group(1):
                                # All rules for each symbol of input alphabet
                                tmp_X.add(rule)
                                break
                    tmp_Q = { re.search(r"'->(.+)$", tmp).group(1) for tmp in tmp_X }
                    for tmp_state_set in Q_m:
                        if tmp_Q <= tmp_state_set:
                            issubset = True
                            if not { X for tmp in new_Q_m if X >= tmp}:
                                new_Q_m.add(X)
                    # Division of states
                    if not issubset:
                        # States will be modified
                        changed = True
                        # Set of pairs where first item is rule and second set of states where second state from rule belong
                        tmp_set = set()
                        for rule in tmp_X:
                            for tmp_state_set in Q_m:
                                if re.search(r"'->(.+)$", rule).group(1) in tmp_state_set:
                                    tmp_set.add(tuple([rule,tmp_state_set]))
                        tmp = None
                        X1 = set()
                        X2 = set()
                        # Division
                        for tmp_pair in tmp_set:
                            if not tmp:
                                tmp = tmp_pair[1]
                            if tmp_pair[1] == tmp:
                                X1.add(re.search(r"^(.+?)'", tmp_pair[0]).group(1))
                            else:
                                X2.add(re.search(r"^(.+?)'", tmp_pair[0]).group(1))
                        tmp_new_Q_m = set()
                        for tmp_state_set in Q_m:
                            if not ((X1 | X2) <= tmp_state_set):
                                tmp_new_Q_m.add(tmp_state_set)
                        new_Q_m.clear()
                        new_Q_m = set(tmp_new_Q_m)
                        new_Q_m.add(frozenset(X1))
                        new_Q_m.add(frozenset(X2))
                    # Preparation for next iteration
                    issubset = False
                    tmp_X.clear()
                    tmp_Q.clear()
            # Iteration is stopped when states was not modified
            if not changed:
                break;
            # Assigment new states
            Q_m = set(new_Q_m)
            new_Q_m.clear()
        if frozenset() in Q_m:
            Q_m.remove(frozenset())
        # Find out new start state and new set of final states
        new_start_state = None
        new_final_states = set()
        for tmp_state_set in Q_m:
            if self.START_STATE in tmp_state_set:
                new_start_state = tmp_state_set
            if self.FINAL_STATES & tmp_state_set:
                new_final_states.add(frozenset(tmp_state_set))
        
        # Creating of new starting state after minimization
        new = ""
        for tmp_state in sorted(new_start_state):
            new += tmp_state
            if sorted(new_start_state).index(tmp_state) != len(new_start_state) - 1:
                new += "_"
        self.START_STATE = new
        
        # Creating of new final states set after minimization
        tmp_set = set()
        for tmp_state_set in new_final_states:
            new = ""
            for tmp_state in sorted(tmp_state_set):
                new += tmp_state
                if sorted(tmp_state_set).index(tmp_state) != len(tmp_state_set) - 1:
                    new += "_"
            tmp_set.add(new)
        self.FINAL_STATES = tmp_set
        
        # Creating of new rules set after minimization
        new_rules = set()
        for tmp_rule in self.RULES:
            new_rule = ""
            state1 = re.search(r"^(.+?)'", tmp_rule).group(1)
            tmp_symbol = re.search(r"('.{0,2}')", tmp_rule, re.DOTALL).group(1)
            state2 = re.search(r"'->(.+)$", tmp_rule).group(1)
            new1 = ""
            new2 = ""
            for tmp_state_set in Q_m:
                if state1 in tmp_state_set:
                    for tmp_state in sorted(tmp_state_set):
                        new1 += tmp_state
                        if sorted(tmp_state_set).index(tmp_state) != len(tmp_state_set) - 1:
                            new1 += "_"
                if state2 in tmp_state_set:
                    for tmp_state in sorted(tmp_state_set):
                        new2 += tmp_state
                        if sorted(tmp_state_set).index(tmp_state) != len(tmp_state_set) - 1:
                            new2 += "_"
            new_rule = new1 + tmp_symbol + "->" + new2
            new_rules.add(new_rule)
        self.RULES = new_rules

        # Creating of new states set after minimization
        tmp_set = set()
        for tmp_state_set in Q_m:
            new = ""
            for tmp_state in sorted(tmp_state_set):
                new += tmp_state
                if sorted(tmp_state_set).index(tmp_state) != len(tmp_state_set) - 1:
                    new += "_"
            tmp_set.add(new)
        self.STATES = set(tmp_set)

    def write(self, file = sys.stdout):
        """Method write fintie automaton in normal form into file.
        
        Args:
        file -- handle of open file, default stdout

        """
        
        file.write("(\n{")
        
        sorted_states = sorted(self.STATES)
        for tmp in sorted_states:
            file.write(tmp)
            if sorted_states.index(tmp) != len(sorted_states) - 1:
                file.write(", ")
        file.write("},\n{")

        sorted_input_alphabet = sorted(self.INPUT_ALPHABET)
        for tmp in sorted_input_alphabet:
            file.write(tmp)
            if sorted_input_alphabet.index(tmp) != len(sorted_input_alphabet) - 1:
                file.write(", ")
        file.write("},\n{\n")

        # Dictionary where key is rule without characters ',-,> and value is original rule
        sorted_rules = { re.sub(r"->", r"", re.sub(r"'(.*)'", r"\1", tmp)):tmp for tmp in self.RULES }
        for tmp in sorted(sorted_rules.keys()):
            tmp_state1 = re.search(r"^(.+?)'", sorted_rules[tmp]).group(1)
            tmp_symbol = re.search(r"('.{0,2}')", sorted_rules[tmp], re.DOTALL).group(1)
            tmp_state2 = re.search(r"'->(.+)$", sorted_rules[tmp]).group(1)
            file.write(tmp_state1 + " " + tmp_symbol + " -> " + tmp_state2)
            if sorted(sorted_rules.keys()).index(tmp) != len(sorted_rules.keys()) - 1:
                file.write(",\n")
        file.write("\n},\n" + self.START_STATE + ",\n{")

        sorted_final_states = sorted(self.FINAL_STATES)
        for tmp in sorted_final_states:
            file.write(tmp)
            if sorted_final_states.index(tmp) != len(sorted_final_states) - 1:
                file.write(", ")
        
        file.write("}\n)")

    def analyze(self, string):
        """Method analyze input string if is accepted language of finite automaton.
        Args:
        string -- input string

        Returns:
        0 string is not accepted language
        1 string is accepted language

        Exceptions:
        ValueError -- string contain symbol which is not from input alphabet

        """

        tmp_state = self.START_STATE

        for char in string:
            char = "'" + char + "'"
            if char not in self.INPUT_ALPHABET:
                raise ValueError("Symbol is not from input alphabet")
            for rule in self.RULES:
                tmp_left_side = re.search(r"^(.+?)->", rule, re.DOTALL).group(1)
                tmp_state1 = re.search(r"'->(.+)$", rule).group(1)
                if tmp_state + char == tmp_left_side:
                    tmp_state = tmp_state1
                    break
        if tmp_state in self.FINAL_STATES:
            return 1
        else:
            return 0

class InputFaError(Exception):
    """
    This exceptions is raised when input file does not contain valid finite automaton.
    """
    pass

class State(object):
    """ States required in parsing input. """
    START_STATE_SET = 1
    FIVE_TUPLE_STATE_SET = 2
    COMMENT = 3
    STATE_SET = 4
    STATE = 5
    WAIT_STATE_SET = 6
    START_IN_ALPHABET_SET = 7
    FIVE_TUPLE_IN_ALPHABET_SET = 8
    IN_ALPHABET_SET = 9
    IN_CHAR = 10
    IN_MID_CHAR = 11
    IN_END_CHAR = 12
    WAIT_IN_ALPHABET_SET = 13
    APOSTHROPHE = 14
    START_RULE_SET = 15
    FIVE_TUPLE_RULE_SET = 16
    RULE_SET = 17
    RULE1 = 18
    WAIT_RULE_SET_CHAR = 19
    DASH = 20
    RULE2_SET = 21
    RULE2 = 22
    WAIT_RULE_SET = 23
    RBRACKET = 24
    RULE_APOSTHROPHE = 25
    RULE_IN_MID_CHAR = 26
    RULE_IN_END_CHAR = 27
    START_START_STATE = 28
    START_STATE = 29
    FIVE_TUPLE_START_STATE = 31
    START_FIN_STATE_SET = 32
    FIVE_TUPLE_FIN_STATE_SET = 33
    FIN_STATE_SET = 34
    FIN_STATE = 35
    WAIT_FIN_STATE_SET = 36
    END_FIVE_TUPLE = 37

def parse_input(input):
    """Function parse input text.
    Args:
    input -- input text

    Returns:
    tuple of five compoments, where compenen can be set or string

    """

    tmp_fa = [ set(), set(), set(), "" , set() ]
    # Indicates if finite automaton is complete
    complete = False
    # Start state
    state = State.START_STATE_SET
    item = ""
    
    # Reading input by chars
    for char in input:

        if state == State.START_STATE_SET:
            if char == "(":
                state = State.FIVE_TUPLE_STATE_SET
            elif char == " " or char == "\t" or char == "\n":
                state = State.START_STATE_SET
            elif char == "#":
                oldstate = state
                state = State.COMMENT
            else:
                raise InputFaError("Not valid finite automaton input")

        elif state == State.COMMENT:
            if char == "\n":
                state = oldstate
            else:
                state = State.COMMENT
        
        elif state == State.FIVE_TUPLE_STATE_SET:
            if char == "{":
                state = State.STATE_SET
            elif char == " " or char == "\t" or char == "\n":
                state = State.FIVE_TUPLE_STATE_SET
            elif char == "#":
                oldstate = state
                state = State.COMMENT
            else:
                raise InputFaError("Not valid finite automaton input")
        
        elif state == State.STATE_SET:
            if "a" <= char <= "z" or "A" <= char <= "Z":
                item += char
                last = char
                state = State.STATE
            elif char == " " or char == "\t" or char == "\n":
                state = State.STATE_SET
            elif char == "#":
                oldstate = state
                state = State.COMMENT
            elif char == "}":
                state = State.START_IN_ALPHABET_SET
            else:
                raise InputFaError("Not valid finite automaton input")
        
        elif state == State.STATE:
            if "a" <= char <= "z" or "A" <= char <= "Z" or "0" <= char <= "9" or char == "_":
                item += char
                last = char
                state = State.STATE
            elif char == ",":
                if not ("a" <= last <= "z" or "A" <= last <= "Z" or "0" <= last <= "9"):
                    raise InputFaError("Not valid finite automaton input")
                tmp_fa[0].add(item)
                item = ""
                state = State.STATE_SET
            elif char == " " or char == "\t" or char == "\n":
                state = State.WAIT_STATE_SET
            elif char == "}":
                if not ("a" <= last <= "z" or "A" <= last <= "Z" or "0" <= last <= "9"):
                    raise InputFaError("Not valid finite automaton input")
                tmp_fa[0].add(item)
                item = ""
                state = State.START_IN_ALPHABET_SET
            elif char == "#":
                oldstate = State.WAIT_STATE_SET
                state = State.COMMENT
            else:
                raise InputFaError("Not valid finite automaton input")

        elif state == State.WAIT_STATE_SET:
            if char == " " or char == "\t" or char == "\n":
                state = State.WAIT_STATE_SET
            elif char == ",":
                if not ("a" <= last <= "z" or "A" <= last <= "Z" or "0" <= last <= "9"):
                    raise InputFaError("Not valid finite automaton input")
                tmp_fa[0].add(item)
                item = ""
                state = State.STATE_SET
            elif char == "}":
                if not ("a" <= last <= "z" or "A" <= last <= "Z" or "0" <= last <= "9"):
                    raise InputFaError("Not valid finite automaton input")
                tmp_fa[0].add(item)
                item = ""
                state = State.START_IN_ALPHABET_SET
            elif char == "#":
                oldstate = state
                state = State.COMMENT
            else:
                raise InputFaError("Not valid finite automaton input")

        elif state == State.START_IN_ALPHABET_SET:
            if char == ",":
                state = State.FIVE_TUPLE_IN_ALPHABET_SET
            elif char == " " or char == "\t" or char == "\n":
                state = State.START_IN_ALPHABET_SET
            elif char == "#":
                oldstate = state
                state = State.COMMENT
            else:
                raise InputFaError("Not valid finite automaton input")
        
        elif state == State.FIVE_TUPLE_IN_ALPHABET_SET:
            if char == "{":
                state = State.IN_ALPHABET_SET
            elif char == " " or char == "\t" or char == "\n":
                state = State.FIVE_TUPLE_IN_ALPHABET_SET
            elif char == "#":
                oldstate = state
                state = State.COMMENT
            else:
                raise InputFaError("Not valid finite automaton input")

        elif state == State.IN_ALPHABET_SET:
            if char == "'":
                item += char
                state = State.IN_MID_CHAR
            elif char == " " or char == "\t" or char == "\n":
                state = State.IN_ALPHABET_SET
            elif char == "#":
                oldstate = state
                state = State.COMMENT
            elif char == "}":
                state = State.START_RULE_SET
            else:
                raise InputFaError("Not valid finite automaton input")
        
        elif state == State.IN_MID_CHAR:
            if char == "'":
                item += char
                state = State.APOSTHROPHE
            else:
                item += char
                state = State.IN_END_CHAR;

        elif state == State.APOSTHROPHE:
            if char == "'":
                item += char
                state = State.IN_END_CHAR
            elif char == " " or char == "\t" or char == "\n" or char == ",":
                tmp_fa[1].add(item)
                item = ""
                state = State.WAIT_IN_ALPHABET_SET
            else:
                raise InputFaError("Not valid finite automaton input")

        elif state == State.IN_END_CHAR:
            if char == "'":
                item += char
                tmp_fa[1].add(item)
                item = ""
                state = State.WAIT_IN_ALPHABET_SET
            else:
                raise InputFaError("Not valid finite automaton input")

        elif state == State.WAIT_IN_ALPHABET_SET:
            if char == " " or char == "\t" or char == "\n":
                state = State.WAIT_IN_ALPHABET_SET
            elif char == ",":
                state = State.IN_ALPHABET_SET
            elif char == "}":
                state = State.START_RULE_SET
            elif char == "#":
                oldstate = state
                state = State.COMMENT
            else:
                raise InputFaError("Not valid finite automaton input")
        
        elif state == State.START_RULE_SET:
            if char == ",":
                state = State.FIVE_TUPLE_RULE_SET
            elif char == " " or char == "\t" or char == "\n":
                state = State.START_RULE_SET
            elif char == "#":
                oldstate = state
                state = State.COMMENT
            else:
                raise InputFaError("Not valid finite automaton input")

        elif state == State.FIVE_TUPLE_RULE_SET:
            if char == "{":
                state = State.RULE_SET
            elif char == " " or char == "\t" or char == "\n":
                state = State.FIVE_TUPLE_RULE_SET
            elif char == "#":
                oldstate = state
                state = State.COMMENT
            else:
                raise InputFaError("Not valid finite automaton input")

        elif state == State.RULE_SET:
            if "a" <= char <= "z" or "A" <= char <= "Z":
                item += char
                last = char
                state = State.RULE1
            elif char == " " or char == "\t" or char == "\n":
                state = State.RULE_SET
            elif char == "#":
                oldstate = state
                state = State.COMMENT
            elif char == "}":
                state = State.START_START_STATE
            else:
                raise InputFaError("Not valid finite automaton input")

        elif state == State.RULE1:
            if "a" <= char <= "z" or "A" <= char <= "Z" or "0" <= char <= "9" or char == "_":
                item += char
                last = char
                state = State.RULE1
            elif char == "'":
                item += char
                if not ("a" <= last <= "z" or "A" <= last <= "Z" or "0" <= last <= "9"):
                    raise InputFaError("Not valid finite automaton input")
                state = State.RULE_IN_MID_CHAR
            elif char == " " or char == "\t" or char == "\n":
                state = State.WAIT_RULE_SET_CHAR
            elif char == "#":
                oldstate = State.WAIT_RULE_SET_CHAR
                state = State.COMMENT
            else:
                raise InputFaError("Not valid finite automaton input")

        elif state == State.WAIT_RULE_SET_CHAR:
            if char == " " or char == "\t" or char == "\n":
                state = State.WAIT_RULE_SET_CHAR
            elif char == "'":
                item += char
                if not ("a" <= last <= "z" or "A" <= last <= "Z" or "0" <= last <= "9"):
                    raise InputFaError("Not valid finite automaton input")
                state = State.RULE_IN_MID_CHAR
            elif char == "#":
                oldstate = state
                state = State.COMMENT
            else:
                raise InputFaError("Not valid finite automaton input")

        elif state == State.RULE_IN_MID_CHAR:
            if char == "'":
                item += char
                state = State.RULE_APOSTHROPHE
            else:
                item += char
                state = State.RULE_IN_END_CHAR;

        elif state == State.RULE_APOSTHROPHE:
            if char == "'":
                item += char
                state = State.RULE_IN_END_CHAR
            elif char == " " or char == "\t" or char == "\n" or char == ",":
                state = State.DASH
            elif char == "-":
                item += char
                state = State.RBRACKET
            else:
                raise InputFaError("Not valid finite automaton input")

        elif state == State.RULE_IN_END_CHAR:
            if char == "'":
                item += char
                state = State.DASH
            else:
                raise InputFaError("Not valid finite automaton input")

        elif state == State.DASH:
            if char == " " or char == "\t" or char == "\n":
                state = State.DASH
            elif char == "-":
                item += char
                state = State.RBRACKET
            elif char == "#":
                oldstate = state
                state = State.COMMENT
            else:
                raise InputFaError("Not valid finite automaton input")

        elif state == State.RBRACKET:
            if char == ">":
                item += char
                state = State.RULE2_SET
            else:
                raise InputFaError("Not valid finite automaton input")

        elif state == State.RULE2_SET:
            if "a" <= char <= "z" or "A" <= char <= "Z":
                item += char
                last = char
                state = State.RULE2
            elif char == " " or char == "\t" or char == "\n":
                state = State.RULE2_SET
            elif char == "#":
                oldstate = state
                state = State.COMMENT
            else:
                raise InputFaError("Not valid finite automaton input")
        
        elif state == State.RULE2:
            if "a" <= char <= "z" or "A" <= char <= "Z" or "0" <= char <= "9" or char == "_":
                item += char
                last = char
                state = State.RULE2
            elif char == ",":
                if not ("a" <= last <= "z" or "A" <= last <= "Z" or "0" <= last <= "9"):
                    raise InputFaError("Not valid finite automaton input")
                tmp_fa[2].add(item)
                item = ""
                state = State.RULE_SET
            elif char == " " or char == "\t" or char == "\n":
                state = State.WAIT_RULE_SET
            elif char == "}":
                if not ("a" <= last <= "z" or "A" <= last <= "Z" or "0" <= last <= "9"):
                    raise InputFaError("Not valid finite automaton input")
                tmp_fa[2].add(item)
                item = ""
                state = State.START_START_STATE
            elif char == "#":
                oldstate = State.RULE4
                state = State.COMMENT
            else:
                raise InputFaError("Not valid finite automaton input")

        elif state == State.WAIT_RULE_SET:
            if char == " " or char == "\t" or char == "\n":
                state = State.WAIT_RULE_SET
            elif char == ",":
                if not ("a" <= last <= "z" or "A" <= last <= "Z" or "0" <= last <= "9"):
                    raise InputFaError("Not valid finite automaton input")
                tmp_fa[2].add(item)
                item = ""
                state = State.RULE_SET
            elif char == "}":
                if not ("a" <= last <= "z" or "A" <= last <= "Z" or "0" <= last <= "9"):
                    raise InputFaError("Not valid finite automaton input")
                tmp_fa[2].add(item)
                item = ""
                state = State.START_START_STATE
            elif char == "#":
                oldstate = state
                state = State.COMMENT
            else:
                raise InputFaError("Not valid finite automaton input")

        elif state == State.START_START_STATE:
            if char == ",":
                state = State.FIVE_TUPLE_START_STATE
            elif char == " " or char == "\t" or char == "\n":
                state = State.START_START_STATE
            elif char == "#":
                oldstate = state
                state = State.COMMENT
            else:
                raise InputFaError("Not valid finite automaton input")
        
        elif state == State.FIVE_TUPLE_START_STATE:
            if "a" <= char <= "z" or "A" <= char <= "Z":
                item += char
                last = char
                state = State.START_STATE
            elif char == " " or char == "\t" or char == "\n":
                state = State.FIVE_TUPLE_START_STATE
            elif char == "#":
                oldstate = state
                state = State.COMMENT
            else:
                raise InputFaError("Not valid finite automaton input")
        
        elif state == State.START_STATE:
            if "a" <= char <= "z" or "A" <= char <= "Z" or "0" <= char <= "9" or char == "_":
                item += char
                last = char
                state = State.START_STATE
            elif char == ",":
                if not ("a" <= last <= "z" or "A" <= last <= "Z" or "0" <= last <= "9"):
                    raise InputFaError("Not valid finite automaton input")
                tmp_fa[3] = item
                item = ""
                state = State.FIVE_TUPLE_FIN_STATE_SET
            elif char == " " or char == "\t" or char == "\n":
                if not ("a" <= last <= "z" or "A" <= last <= "Z" or "0" <= last <= "9"):
                    raise InputFaError("Not valid finite automaton input")
                tmp_fa[3] = item
                item = ""
                state = State.START_FIN_STATE_SET
            elif char == "#":
                if not ("a" <= last <= "z" or "A" <= last <= "Z" or "0" <= last <= "9"):
                    raise InputFaError("Not valid finite automaton input")
                tmp_fa[3] = item
                item = ""
                oldstate = State.START_FIN_STATE_SET
                state = State.COMMENT
            else:
                raise InputFaError("Not valid finite automaton input")

        elif state == State.START_FIN_STATE_SET:
            if char == ",":
                state = State.FIVE_TUPLE_FIN_STATE_SET
            elif char == " " or char == "\t" or char == "\n":
                state = State.START_FIN_STATE_SET
            elif char == "#":
                oldstate = state
                state = State.COMMENT
            else:
                raise InputFaError("Not valid finite automaton input")

        elif state == State.FIVE_TUPLE_FIN_STATE_SET:
            if char == "{":
                state = State.FIN_STATE_SET
            elif char == " " or char == "\t" or char == "\n":
                state = State.FIVE_TUPLE_FIN_STATE_SET
            elif char == "#":
                oldstate = state
                state = State.COMMENT
            else:
                raise InputFaError("Not valid finite automaton input")
        
        elif state == State.FIN_STATE_SET:
            if "a" <= char <= "z" or "A" <= char <= "Z":
                item += char
                last = char
                state = State.FIN_STATE
            elif char == " " or char == "\t" or char == "\n":
                state = State.FIN_STATE_SET
            elif char == "#":
                oldstate = state
                state = State.COMMENT
            elif char == "}":
                state = State.END_FIVE_TUPLE
            else:
                raise InputFaError("Not valid finite automaton input")
        
        elif state == State.FIN_STATE:
            if "a" <= char <= "z" or "A" <= char <= "Z" or "0" <= char <= "9" or char == "_":
                item += char
                last = char
                state = State.FIN_STATE
            elif char == ",":
                if not ("a" <= last <= "z" or "A" <= last <= "Z" or "0" <= last <= "9"):
                    raise InputFaError("Not valid finite automaton input")
                tmp_fa[4].add(item)
                item = ""
                state = State.FIN_STATE_SET
            elif char == " " or char == "\t" or char == "\n":
                state = State.WAIT_FIN_STATE_SET
            elif char == "}":
                if not ("a" <= last <= "z" or "A" <= last <= "Z" or "0" <= last <= "9"):
                    raise InputFaError("Not valid finite automaton input")
                tmp_fa[4].add(item)
                item = ""
                state = State.END_FIVE_TUPLE
            elif char == "#":
                oldstate = State.WAIT_FIN_STATE_SET
                state = State.COMMENT
            else:
                raise InputFaError("Not valid finite automaton input")

        elif state == State.WAIT_FIN_STATE_SET:
            if char == " " or char == "\t" or char == "\n":
                state = State.WAIT_FIN_STATE_SET
            elif char == ",":
                if not ("a" <= last <= "z" or "A" <= last <= "Z" or "0" <= last <= "9"):
                    raise InputFaError("Not valid finite automaton input")
                tmp_fa[4].add(item)
                item = ""
                state = State.FIN_STATE_SET
            elif char == "}":
                if not ("a" <= last <= "z" or "A" <= last <= "Z" or "0" <= last <= "9"):
                    raise InputFaError("Not valid finite automaton input")
                tmp_fa[4].add(item)
                item = ""
                state = State.END_FIVE_TUPLE
            elif char == "#":
                oldstate = state
                state = State.COMMENT
            else:
                raise InputFaError("Not valid finite automaton input")

        elif state == State.END_FIVE_TUPLE:
            if char == ")":
                complete = True
                state = State.END_FIVE_TUPLE
            elif char == " " or char == "\t" or char == "\n":
                state = State.END_FIVE_TUPLE
            elif char == "#":
                oldstate = state
                state = State.COMMENT
            else:
                raise InputFaError("Not valid finite automaton input")

    # Finite automaton must be complete, so must contain all components
    if not complete:
        raise InputFaError("Not valid finite automaton input")

    return tuple(tmp_fa)
